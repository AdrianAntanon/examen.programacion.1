package prueba.examen;

import java.util.Scanner;

public class Bucles_10 {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		
		int filas=0, i=0, j=0, x=0;
		
		System.out.println("�De cu�ntas filas te gustar�a que fuese la pir�mide? ");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, introduce un entero por favor");
			lector.next();
		}
		
		filas=lector.nextInt();
		
		while(filas<1) {
			System.out.println("Como m�nimo introduce 2 filas, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, introduce un entero por favor");
				lector.next();
			}
			filas=lector.nextInt();
		}
		lector.close();
		
		
		for(i=1;i<=filas;i++) {
			
			for(j=1;j<=(filas-i);j++){
				
				System.out.print(" ");
				
			}for(x=1;x<=(i*2)-1;x++) {
				
				System.out.print("*");
				
			}
			System.out.println("");
		}
	}

}
