package prueba.examen;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Bucles_11 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
	    int numero, suma1, suma2, resta1, resta2, multi1, multi2;
	    int resultado=0;
	    double resultadoDivision=0;
	    double divi1, divi2;
	    int i=0;
	    
	    
        do {

            System.out.println("CALCULADORA\n"
                    + "Men� principal\n"
                    + "1-sumar\n"
                    + "2-restar\n"
                    + "3-multiplicar\n"
                    + "4-dividir\n"
                    + "0-salir\n"
                    + "Introduzca un n�mero entero seg�n lo que quiera realizar, por favor:");
            while(!(lector.hasNextInt())) {
            	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
            	lector.next();
            }
            numero=lector.nextInt();
            while(numero < 0 && numero > 4) {
            	System.out.println("Esa no es ninguna opci�n disponible, vuelve a introducirlo, por favor");
            	while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
            	numero=lector.nextInt();
            }


            if(numero==1) {
                
                System.out.println("Introduzca los dos n�meros enteros que desea sumar, por favor:");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                suma1=lector.nextInt();
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                suma2=lector.nextInt();
                resultado=suma1+suma2;
                
                System.out.println("El resultado de la suma es: " + resultado);
                System.out.println("Pulse 1 para volver a la calculadora, por favor");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                i=lector.nextInt();
                while(!(i==1)){
                    System.out.println("Vuelve a introducir el n�mero, por favor, recuerda que solo 1 te devuelve a la calculadora: ");
                    while(!(lector.hasNextInt())) {
                    	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                    	lector.next();
                    }
                    i=lector.nextInt();
                }


            }else if(numero==2) {
                
                System.out.println("Introduzca los dos n�meros enteros que desea restar, por favor:");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                resta1=lector.nextInt();
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                resta2=lector.nextInt();
                resultado=resta1-resta2;
                
                System.out.println("El resultado de la resta es: " + resultado);
                System.out.println("Pulse 2 para volver a la calculadora, por favor");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                i=lector.nextInt();
                while(!(i==2)){
                    System.out.println("Vuelve a introducir el n�mero, por favor, recuerda que solo 2 te devuelve a la calculadora: ");
                    while(!(lector.hasNextInt())) {
                    	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                    	lector.next();
                    }
                    i=lector.nextInt();
                }


            }else if(numero==3) {
                
                System.out.println("Introduzca los dos n�meros enteros que desea multiplicar, por favor:");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                multi1=lector.nextInt();
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                multi2=lector.nextInt();
               resultado=multi1*multi2;
                System.out.println("El resultado de la multiplicaci�n es: " + resultado);
                
                System.out.println("Pulse 3 para volver a la calculadora, por favor");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                i=lector.nextInt();
                while(!(i==3)){
                    System.out.println("Vuelve a introducir el n�mero, por favor, recuerda que solo 3 te devuelve a la calculadora: ");
                    while(!(lector.hasNextInt())) {
                    	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                    	lector.next();
                    }
                    i=lector.nextInt();
                }

            }else if(numero==4) {
               
                System.out.println("Introduzca los dos n�meros enteros que desea dividir, por favor:");
                while(!(lector.hasNextDouble())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                divi1=lector.nextDouble();
                while(!(lector.hasNextDouble())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                divi2=lector.nextDouble();
                while(divi2==0) {
                    System.out.println("No es posible realizar esta operaci�n al ser 0 el divisor. \n" +
                            "Vuelva a introducir el segundo n�mero, por favor ");
                    while(!(lector.hasNextDouble())) {
                    	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                    	lector.next();
                    }
                    divi2=lector.nextDouble();
                }
                DecimalFormat df = new DecimalFormat("#.00");
                resultadoDivision=divi1/divi2;
                System.out.println("El resultado de la divisi�n es: " + df.format(resultadoDivision));
               
                System.out.println("Pulse 4 para volver a la calculadora, por favor");
                while(!(lector.hasNextInt())) {
                	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                	lector.next();
                }
                i=lector.nextInt();
                
                while(!(i==4)){
                	
                    System.out.println("Vuelve a introducir el n�mero, por favor, recuerda que solo 4 te devuelve a la calculadora: ");
                    while(!(lector.hasNextInt())) {
                    	System.out.println("Es no es un n�mero correcto, vuelve a introducirlo, por favor");
                    	lector.next();
                    }
                    i=lector.nextInt();
                    
                }

            }else if(numero==0) {
            	
                System.out.println("Ha salido del programa, que tenga un buen d�a.");
                
            }

        }while(numero!=0);
        lector.close();

	}

}
