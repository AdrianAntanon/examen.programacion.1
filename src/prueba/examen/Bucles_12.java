package prueba.examen;

import java.util.Scanner;

public class Bucles_12 {
	public static final int DIEZ=10;
	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int num=0, i=0, copiaNum=0;
		
		System.out.println("Introduce un n�mero entero m�ltiplo de "+ DIEZ +", por favor.");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a introducirlo, por favor");
			lector.next();
		}
		num=lector.nextInt();
		while(!(num%DIEZ==0)) {
			System.out.println("El n�mero introducido no es m�ltiplo de 10, vuelva a introducirlo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducirlo, por favor");
				lector.next();
			}
			num=lector.nextInt();
		}
		lector.close();
		copiaNum=num;
		
		for(i=0;num%DIEZ==0;i++) {
			num=num/DIEZ;
		}
		
		System.out.println("El n�mero introducido: " + copiaNum + " es " + num + " elevado a la " + DIEZ  + " un total de " + i + " veces");
		

	}

}
