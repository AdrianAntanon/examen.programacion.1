package prueba.examen;

public class Bucles_13 {
	public static final int muerte = 10000000;
	public static void main(String[] args) {
		
		System.out.println("Vamos a comprobar cu�l es el ciclo de vida de una bacteria, la bacteria empieza siendo 1 c�lula y muere cuando son m�s de 10000000.\n"
				+ "Tambi�n tiene la peculiaridad de que cada 180 segundos se puede multiplicar.\n"
				+ "\n"
				+ "CALCULANDO...\n"
				+ "");
		
		double min=0, bacteria=1;
		double hora = 0;
		for(min=0;bacteria<muerte;min+=3) {
			bacteria=bacteria*2;
		}
		
		hora=min/60;
		
		System.out.println("Seg�n los c�lculos del ordenador podemos confirmar que la esperaza de vida es de unos  "+ min + " minutos o , dicho de otro modo, " + hora + " horas");

	}

}
