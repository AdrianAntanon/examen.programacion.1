package prueba.examen;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Bucles_16 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		DecimalFormat df = new DecimalFormat ("#.00");
		
		int num=0, i=0, mayor=0, menor=mayor;
		double mediana=0;
		int [] array;
		
		System.out.println("�Cu�ntos n�meros le gustar�a introducir?");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
			lector.next();
		}
		num=lector.nextInt();
		
		while(num<1) {
			System.out.println("El n�mero no puede ser inferior a 1, vuelve a introducirlo, por favor.");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
				lector.next();
			}
			num=lector.nextInt();
		}
		
		array= new int [num];
		
		for(i=0;i<num;i++) {
			
			System.out.println("Introduzca el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
				lector.next();
			}
			array[i]=lector.nextInt();
			if(array[i]>=mayor) {
				mayor=array[i];
			}else if(array[i]<=menor) {
				menor=array[i];
			}
			
			mediana=array[i]+mediana;
			
		}
		
		lector.close();
		
		mediana=mediana/num;
		
		System.out.println("El m�mero m�s alto introducido es: " + mayor + " y el m�s peque�o: " + menor + ", la mediana de la serie de n�meors introducidos es " + df.format(mediana));

	}

}
