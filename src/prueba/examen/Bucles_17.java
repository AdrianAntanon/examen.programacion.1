package prueba.examen;

import java.util.Scanner;

public class Bucles_17 {

	public static void main(String[] args) {
		
		Scanner lector=new Scanner(System.in);
		
		int columnas=0, asteriscos=0, contador=0;
		
        System.out.println("Por favor, seleciona el n�mero de filas que quieres que contenga el cuadrado: ");
        while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
			lector.next();
		}
        columnas=lector.nextInt();
        while(columnas<3) {
			System.out.println("El n�mero no puede ser inferior a 3, vuelve a introducirlo, por favor.");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
				lector.next();
			}
			columnas=lector.nextInt();
		}

        asteriscos=columnas;
        
        //El primer bucle es para dibujar la l�nea de arriba
        
        for(contador=0;contador<asteriscos;contador++) {
            System.out.print("*");
        }System.out.println("");
        
        //El segundo bucle sirve para introducir las l�neas de en medio
        
        for(contador=1;contador<=(asteriscos-3);contador++){
            System.out.print("*");
            
            // el tercer bucle, o bucle anidado, es para que fije los espacios entre el primer y �ltimo aster�sco
            
            for(int i=0;(asteriscos-2)>i;i++) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println("");
        }
        //El �ltimo  bucle es para dibujar la l�nea de abajo, es el primer FOR y el �ltimo que hacemos es LO MISMO
        
        for(contador=0;contador<asteriscos;contador++) {
            System.out.print("*");
        }
        lector.close();

		
	}

}
