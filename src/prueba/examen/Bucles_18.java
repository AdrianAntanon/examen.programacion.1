package prueba.examen;

import java.util.Scanner;

public class Bucles_18 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int num=0, i=1;
		
		System.out.println("Introduzca un n�mero y le mostraremos por pantalla todos los divisores que tiene desde 1 hasta llegar al n�mero");
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelva a introducirlo, por favor");
			lector.next();
		}
		num=lector.nextInt();
		while(num<1) {
			System.out.println("El n�mero no puede ser negativo o 0, vuelva a introducirlo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelva a introducirlo, por favor");
				lector.next();
			}
			num=lector.nextInt();
		}
		lector.close();
		
		System.out.println("Los divisores de " + num +" son: ");
		System.out.print("[ ");
		for(i=1;i<=num;i++) {
			if(num%i==0) {
				System.out.print(i + " ");
			}
		}
		System.out.println("]");
		

	}

}
