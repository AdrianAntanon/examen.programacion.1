package prueba.examen;

import java.util.Scanner;

public class Bucles_19 {

	public static void main(String[] args) {

		Scanner lector = new Scanner (System.in);
		
		int num=0, i=0, negativo=0, contador=0;
		int [] array;
		
		System.out.println("�Cu�ntos n�meros le gustar�a introducir?");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
			lector.next();
		}
		num=lector.nextInt();
		
		while(num<1) {
			System.out.println("El n�mero no puede ser inferior a 1, vuelve a introducirlo, por favor.");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
				lector.next();
			}
			num=lector.nextInt();
		}
		
		array= new int [num];
		
		for(i=0;i<num;i++) {
			
			System.out.println("Introduzca el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducir el n�mero.");
				lector.next();
			}
			array[i]=lector.nextInt();
			if(array[i]<negativo) {
				contador++;
			}
			
		}
		lector.close();
		System.out.println("La cantidad de n�meros negativos introducidos es de: " + contador);
		
		
	}

}
