package prueba.examen;

import java.util.Scanner;

public class Bucles_2 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int [] array = new int [2];
		int i=0;
		
		System.out.println("El ejercicio consiste en introducir dos n�meros y mostrar el intervalo entre los dos, el segundo nunca podr� ser igual o inferior al primero");
		
		for(i=0;i<2;i++) {
			System.out.println("Introduce el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Otra vez, no es un entero");
				lector.hasNext();
			}
			array[i]=lector.nextInt();
			
		}
		while(array[0]>array[1]) {
			System.out.println("El segundo n�mero nunca podr� ser inferior o igual al primero, vuelve a introducir el segundo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Otra vez, no es un entero");
				lector.hasNext();
			}
			array[1]=lector.nextInt();
		}
		System.out.print("El intervalo es el siguiente: [ ");
		for(i=array[0]+1;i<array[1];i++) {
			
			System.out.print(i + " ");
		}
		System.out.println("]");
		lector.close();

	}

}
