package prueba.examen;

public class Bucles_20 {

	public static void main(String[] args) {
		
		System.out.println("Quedar�an de la siguiente forma las cadenas creadas mediante bucles: ");

        for(int i=1;i<=10;i++){
            if(i<11){
                System.out.print(i + "    " );
            }else {
                System.out.print(i + "   ");
            }
        }System.out.println("");

        for(int j=2;j<=20;j+=2){
            if (j<10){
                System.out.print(j + "    ");
            }else{
                System.out.print(j + "   ");
            }

        }System.out.println("");

        for(int x=20;x<40;x+=2){
            System.out.print(x + "   ");

        }System.out.println("");

        for(int r=10;r<=30;r+=4){
            System.out.print(r + "   ");

        }System.out.println("");

        for(int s=40;s>=0;s-=5){
            if(s==10 || s==5){
                System.out.print(s + "   " );
            }else if(s==0){
                System.out.print(" " + s );
            }else {
                System.out.print(s + "   ");
            }

        }

	}

}
