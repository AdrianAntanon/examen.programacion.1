package prueba.examen;

import java.util.Scanner;

public class Bucles_3 {

	public static void main(String[] args) {
		
	Scanner lector = new Scanner (System.in);
		
		int [] array = new int [2];
		int i=0;
		
		System.out.println("El ejercicio consiste en introducir dos n�meros y mostrar de los dos, el segundo nunca podr� ser igual o inferior al primero");
		
		for(i=0;i<2;i++) {
			System.out.println("Introduce el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Otra vez, no es un entero");
				lector.hasNext();
			}
			array[i]=lector.nextInt();
			
		}
		while(array[0]>array[1]) {
			System.out.println("El segundo n�mero nunca podr� ser inferior o igual al primero, vuelve a introducir el segundo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Otra vez, no es un entero");
				lector.hasNext();
			}
			array[1]=lector.nextInt();
		}
		lector.close();
		int suma = array[0]+array[1];
		
		System.out.println("La suma de " + array[0] + " y " + array[1] + " es " + suma);

	}

}
