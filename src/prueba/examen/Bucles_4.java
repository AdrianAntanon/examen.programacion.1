package prueba.examen;

import java.util.Scanner;

public class Bucles_4 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int i=0;
		int [] array = new int [2];
		
		for(i=0;i<2;i++) {
			System.out.println("Introduce el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es v�lido, vuelva a introducirlo, por favor");
				lector.next();
			}
			array[i]=lector.nextInt();
			
			}
		while(array[0]>=array[1]) {
			System.out.println("El segundo n�mero nunca podr� ser m�s peque�o o igual que el primero, vuelva a introducirlo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es v�lido, vuelva a introducirlo, por favor");
				lector.next();
			}
			array[1]=lector.nextInt();
		
		}
		System.out.println("La lista de n�meros enteros que son m�ltiples del primer n�mero y son m�s peque�os o iguales que el segundo n�mero son: ");
		System.out.print("[ ");
		for(i=array[0];i<=array[1];i++) {
			if(i%array[0]==0) {
				System.out.print( i + " ");
			}
		}
		System.out.println("]");
		
		lector.close();

	}

}
