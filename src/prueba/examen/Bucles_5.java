package prueba.examen;

import java.util.Scanner;

public class Bucles_5 {

	public static void main(String[] args) {

		Scanner lector = new Scanner (System.in);
		int j=2, copia=0, num=0;
		
		System.out.println("Introduce un n�mero entero superior a 1, por favor");
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a introducirlo, por favor");
			lector.next();
		}
		
		num = lector.nextInt();
		
		while(num<2) {
			System.out.println("Recuerda que el n�mero introducido debe ser superior a 1, vuelve a meterlo otra vez, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelve a introducirlo, por favor");
				lector.next();
			}
			num=lector.nextInt();
		}
		lector.close();
		
		while(j<num) {
			if(num%j==0) {
				copia=j;
			}
			j++;
		}
		
		if(copia>1) {
			System.out.println("El divisor m�s grande sin ser �l mismo ni 1 es: " + copia);
		}else {
			System.out.println("No se ha encontrado ning�n divisor que sea diferente de 1 y �l mismo, m�s suerte la pr�xima vez");
		}
		
		
	}

}
