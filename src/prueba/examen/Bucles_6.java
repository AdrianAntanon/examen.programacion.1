package prueba.examen;

import java.util.Scanner;

public class Bucles_6 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int num, cifras=0, copiaNum;
		
		System.out.println("Introduce un n�mero entero para saber cu�ntas cifras tiene (Ej: 7 tiene 1 cifra y 700 tiene 3 cifras");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un entero, vuelve a introducirlo, por favor");
			lector.hasNext();
		}
		num=lector.nextInt();
		copiaNum=num;
		
		if(num==0) {
			cifras++;
		}
		while(num!=0){
			num=num/10;
			cifras++;
			
		}
		
		System.out.println("El n�mero " + copiaNum + " tiene " + cifras + " cifra/s");
		
		lector.close();

		
	}

}
