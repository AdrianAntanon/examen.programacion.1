package prueba.examen;

import java.util.Scanner;

public class Bucles_7 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		int num=0, suma=0, i=0;
		System.out.println("Introduce un nombre entero, por favor");
		while(!(lector.hasNextInt())) {
			System.out.println("Introduce un nombre entero, por favor");
			lector.next();
		}
		num=lector.nextInt();
		lector.close();
		System.out.print("La lista de n�meros que ser� necesarios sumar sin llegar a valer lo mismo ni pasarse es la siguiente: \n"
				+ "[ ");
		for(i=0;(suma+i)<num;i++) {
			suma=suma+i;
			System.out.print(i + " ");
		}
		
		System.out.println("]\n"
				+ "Y la suma de todos ellos ofrece un valor de " + suma);
		
		

	}

}
