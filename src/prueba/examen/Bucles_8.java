package prueba.examen;

import java.util.Scanner;

public class Bucles_8 {

	public static void main(String[] args) {
		

		Scanner lector = new Scanner (System.in);
		
		int i=0;
		int [] array = new int [2];
		
		for(i=0;i<2;i++) {
			System.out.println("Introduce el n�mero " + (i+1) + ", por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es v�lido, vuelva a introducirlo, por favor");
				lector.next();
			}
			array[i]=lector.nextInt();
			
			}
		while(array[0]>=array[1]) {
			System.out.println("El segundo n�mero nunca podr� ser m�s peque�o o igual que el primero, vuelva a introducirlo, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es v�lido, vuelva a introducirlo, por favor");
				lector.next();
			}
			array[1]=lector.nextInt();
		
		}
		lector.close();
		System.out.println("La lista decreciente de "+ array[1] + " hasta " + array[0] +" ser�a la siguiente: ");
		
		System.out.print("[ ");
		for(i=array[1]-1;i>array[0];i--) {
			System.out.print(i + " ");
		}
		System.out.println("]");

	}

}
