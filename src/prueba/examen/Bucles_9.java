package prueba.examen;

import java.util.Scanner;

public class Bucles_9 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int filas=0, i=0, j=0;
		
		System.out.println("�De cu�ntas filas te gustar�a que fuese el triangulo? ");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, introduce un entero por favor");
			lector.next();
		}
		
		filas=lector.nextInt();
		
		while(filas<1) {
			System.out.println("Como m�nimo introduce 2 filas, por favor");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, introduce un entero por favor");
				lector.next();
			}
			filas=lector.nextInt();
		}
		lector.close();
		
		for(i=0;i<filas;i++) {
			System.out.print("a");
			for(j=0;j<i;j++){
				System.out.print("a");
			}
			System.out.println("");
		}

	}

}
