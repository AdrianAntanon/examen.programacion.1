package prueba.examen;

import java.util.Scanner;

public class Condicionales_1 {
	public static final int negativo = -1;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner lector = new Scanner(System.in);
		
		int numero;
		System.out.println("Introduce un n�mero real, por favor");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero real, vuelva a introducirlo, por favor ");
			
			lector.next();			
		}
		
		numero=lector.nextInt();
		lector.close();
		
		System.out.println("El n�mero en con signo negativo ser�a " + (numero*negativo));
		
		
	}

}
