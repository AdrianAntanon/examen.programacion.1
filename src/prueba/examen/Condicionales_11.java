package prueba.examen;

import java.util.Scanner;

public class Condicionales_11 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		double nota=0;
		
		
		System.out.println("Introduce la nota que tienes en la asignatura de Programaci�n");
		
		while(!(lector.hasNextDouble())) {
			System.out.println("Esa es imposible que sea tu nota, vuelva a introducirla, por favor");
			lector.next();
			
			
		}
		nota=lector.nextDouble();
		
		while(nota < 0 || nota > 10) {
			System.out.println("Es imposible sacar m�s de un 10 o menos de un 0, vuelve a introducir la nota ");
			while(!(lector.hasNextDouble())) {
				System.out.println("Esa es imposible que sea tu nota, vuelva a introducirla, por favor");
				lector.next();
			}
			nota=lector.nextDouble();
		}
		
		if(nota<5) {
			System.out.println("SUSPENDIDO IN�TIL");
		}else if(nota>=5 && nota<=6.5) {
			System.out.println("Has obtenido un BIEN");
		}else if(nota >=6.6 && nota<=8.5) {
			System.out.println("Has obtenido un NOTABLE");
		}else if(nota>8.5) {
			System.out.println("Tienes un excelente, yo estar�a orgulloso de eso");
		}
		lector.close();
	}

}
