package prueba.examen;

import java.util.Scanner;

public class Condicionales_13 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		double manzanas = 0;
		
		
		System.out.println("Cu�ntos kg de manzanas has comprado?");
		
		while(!(lector.hasNextDouble())) {
			System.out.println("Eso no son kg de manzanas, vuelve a introducir el n�mero");
			lector.next();
		}
		
		manzanas=lector.nextDouble();
		while(manzanas < 0) {
			System.out.println("�Est�s loco? es imposible comprar menos de 0 kg, vuelve a introducir el n�mero anda.");
			while(!(lector.hasNextDouble())) {
				System.out.println("Eso no son kg de manzanas, vuelve a introducir el n�mero");
				lector.next();
			}
			manzanas=lector.nextDouble();
			
		}
		
		if(manzanas<=2) {
			System.out.println("No has obtenido descuento");
		}else if(manzanas >= 2.01 && manzanas<=5 ) {
			System.out.println("Descuento del 10% pa ti");
		}else if(manzanas>=5.01 && manzanas<=10 ) {
			System.out.println("�Es que compras para todo el mes? pues descuento del 15% va");
		}else {
			System.out.println("JO-DER, me has dejado sin manzanas, te aplico un 20% de descuento y espero no verte m�s.");
		}
		
		lector.close();
	}

}
