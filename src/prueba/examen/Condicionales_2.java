package prueba.examen;

import java.util.Scanner;

public class Condicionales_2 {
	public static final int tres = 3;
	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int i=0;
		int num1=0, num2=0, num3=0;
		
		System.out.println("Introduzca tres n�meros enteros, por favor");
		
		for(i=0;i<tres;i++) {
			
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelva a introducirlo, por favor ");
				
				lector.next();			
			}
			
			if(i==0) {
				num1=lector.nextInt();
			}else if(i==1){
				num2=lector.nextInt();
			}else if(i==2) {
				num3=lector.nextInt();
			}
			
		}
		lector.close();
		System.out.println(num1 + " " + num2 + " " + num3);
		
		if(num2 < num3 && num2>num1) {
			System.out.println("El n�mero " + num2 + " es m�s peque�o que " + num3 + " pero m�s grande que " + num1);
		}else if(num2 > num3 && num2<num1) {
			System.out.println("El n�mero " + num2 + " es m�s grande que " + num3 + " pero m�s peque�o que " + num1);
		}else {
			System.out.println("Resultado inconcluso, da igual, esto es para practicar.");
		}
		
	}

}
