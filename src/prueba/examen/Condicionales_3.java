package prueba.examen;

import java.util.Scanner;

public class Condicionales_3 {
	public static final int dos = 2;
	public static void main(String[] args) {


		Scanner lector = new Scanner (System.in);
		
		int i=0;
		int num1=0, num2=0;
		
		System.out.println("Introduzca dos n�meros enteros, por favor");
		
		for(i=0;i<dos;i++) {
			
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelva a introducirlo, por favor ");
				
				lector.next();			
			}
			
			if(i==0) {
				num1=lector.nextInt();
			}else if(i==1){
				num2=lector.nextInt();
			}
			
		}
		lector.close();
		
		int suma = num1+num2;
		int multi = num1*num2;
		
		
		if(suma > multi) {
			System.out.println("La suma: " + suma + " es mayor que la multiplicaci�n: " + multi);
		}else if(multi>suma) {
			System.out.println("La multiplicaci�n: " + multi + " es mayor que la suma: " + suma);
		}else {
			System.out.println("Suma: " + suma + " y multiplicaci�n: " + multi + " son iguales");
		}
	}

}
