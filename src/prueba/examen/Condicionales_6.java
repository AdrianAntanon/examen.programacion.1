package prueba.examen;

import java.util.Scanner;

public class Condicionales_6 {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		int numero = 0;
		System.out.println("Introduce un n�mero entero, por favor ");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a meterlo");
			lector.next();
		}
		
		numero = lector.nextInt();
		
		if( numero%2 == 0) {
			System.out.println("El n�mero " + numero + " es par ");
		}else {
			System.out.println("El n�mero " + numero + " es impar ");
		}
		lector.close();
	}

}
