package prueba.examen;

import java.util.Scanner;

public class Condicionales_7 {
	public static final int pHora = 30;
	public static final double pMetro = 0.5;
	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		int h1, h2, pBruto=0, pIva;
		
		System.out.println("Introduce las horas trabajadas, por favor");
		
		h1 = lector.nextInt();
		
		System.out.println("Introduce el n�mero de metros instalados, por favor");
		
		h2 = lector.nextInt();
		
		pBruto = (int) pHora*h2;
		
		pBruto = pBruto + (pHora*h1);
		
		pIva = (int) (pBruto * 1.21);
		
		System.out.println("El precio bruto es " + pBruto + " y el precio con iva asciende a " + pIva);
		
		lector.close();

	}

}
