package prueba.examen;

import java.util.Scanner;

public class Condicionales_8 {

	public static final int tres = 3;
	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int i=0;
		int num1=0, num2=0, num3=0;
		
		System.out.println("Introduzca tres n�meros enteros, por favor");
		
		for(i=0;i<tres;i++) {
			
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero entero, vuelva a introducirlo, por favor ");
				
				lector.next();			
			}
			
			if(i==0) {
				num1=lector.nextInt();
			}else if(i==1){
				num2=lector.nextInt();
			}else if(i==2) {
				num3=lector.nextInt();
			}
			
		}
		
		if(num1<num2 && num2<num3) {
			System.out.println("N�meros ordenados");
		}else {
			System.out.println("N�meros desordenados");
		}
		lector.close();

	}

}
