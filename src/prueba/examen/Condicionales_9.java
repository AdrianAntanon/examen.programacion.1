package prueba.examen;

import java.util.Scanner;

public class Condicionales_9 {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		System.out.println("Introduce un a�o para saber si ser� bisiesto, por favor");
		int any=0;
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un a�o correcto, vuelva a introducirlo.");
			lector.next();
		}
		
		any=lector.nextInt();
		
		if(any%4 == 0 && (any&100) != 0 || any%400 == 0) {
			
			System.out.println("Es un a�o bisiesto!");
			
		}else {
			System.out.println("Es un a�o normal, ya tocar� la pr�xima.");
		}
		
		lector.close();

	}

}
