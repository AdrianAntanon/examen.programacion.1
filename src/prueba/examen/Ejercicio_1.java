package prueba.examen;

import java.util.Scanner;

public class Ejercicio_1 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		
		int num=0, i=0, resultado=0;
		
		System.out.println("Introduce un n�mero del 2 al 20 para saber su factorial");
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero, vuelve a introducirlo");
			lector.next();
		}
		num=lector.nextInt();
		while(!(num>=2 && num<=20)) {
			System.out.println("Qu� no has entedido de averiguar el factorial de un n�mero? vuelve a meter un n�mero del 2 al 20");
			while(!(lector.hasNextInt())) {
				System.out.println("Eso no es un n�mero, vuelve a introducirlo");
				lector.next();
			}
			num=lector.nextInt();
		}
		
		resultado=num;
		
		for(i=1;i<num;i++) {
			resultado=i*resultado;
		}
		lector.close();
		
		System.out.println("El factorial de " + num + " es " + resultado);
		
	}

}
