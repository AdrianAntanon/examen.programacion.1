package prueba.examen;

import java.util.Scanner;

public class Ejercicio_2 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int numero, i=0, par=0, impar=0;
		
		System.out.println("Introduce un n�mero entero, por favor");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a meterlo otra vez");
			lector.next();
		}
		numero=lector.nextInt();
		
		for(i=0;i<=numero;i++) {
			
			if(i%2==0) {
				par=par+i;
			}else {
				impar=impar+i;
			}
			
		}
		lector.close();
		System.out.println("La suma de los pares es " + par + " y de los impares es " + impar + " hasta llegar al n�mero " + numero );

	}

}
