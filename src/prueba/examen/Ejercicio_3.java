package prueba.examen;

import java.util.Scanner;

public class Ejercicio_3 {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner (System.in);
		
		int num;
		
		System.out.println("Introduce un n�mero entero, por favor");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a meterlo otra vez");
			lector.next();
		}
		num=lector.nextInt();
		
		if(num%2==0 && num<0) {
			
			System.out.println("El n�mero introducido "+ num + "  es par y negativo ");
			
		}else if(num%2!=0 && num<0) {
			System.out.println("El n�mero introducido "+ num + "  es impar y negativo ");

		}else if(num%2==0 && num>0) {
			
			System.out.println("El n�mero introducido "+ num + "  es par y positivo ");
			
		}else if(num%2!=0 && num>0) {
			System.out.println("El n�mero introducido "+ num + "  es impar y positivo ");

		}
		
		lector.close();

	}

}
