package prueba.examen;

import java.util.Scanner;

public class Ejercicio_4 {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		
		int num1=0, num2=0;

		System.out.println("Introduce el primer n�mero entero, por favor");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a meterlo otra vez");
			lector.next();
		}
		
		num1=lector.nextInt();
		
		System.out.println("Introduce el segundo n�mero entero, por favor");
		
		while(!(lector.hasNextInt())) {
			System.out.println("Eso no es un n�mero entero, vuelve a meterlo otra vez");
			lector.next();
		}
		
		num2=lector.nextInt();
		lector.close();
		
		if(num1==num2) {
			System.out.println("Los n�meros " + num1 + " y " + num2 + " son iguales");
		}else if(num1<num2) {
			System.out.println("El n�mero " + num1 + " es m�s peque�o que el n�mero " + num2);
		}else{
			System.out.println("El n�mero " + num2 + " es m�s peque�o que el n�mero " + num1);
		}

	}

}
