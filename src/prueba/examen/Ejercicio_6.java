package prueba.examen;

import java.util.Scanner;

public class Ejercicio_6 {

	public static void main(String[] args) {
		
		 Scanner lector=new Scanner (System.in);
	        boolean seguir=true;
	        int par=0, impar=0, positivo=0, negativo=0, SumaPositivos=0, SumaNegativos=0, a=0, b=0, c=0, d=0, SumaParP=0, SumaImparP=0, SumaParN=0, SumaImparN=0;
	        double mediana=0;
	        int i=1;
	        do {
	            System.out.println("Introduce el n�mero " + i + ", por favor: ");
	            int num=lector.nextInt();
	            mediana=mediana+num;
	            i++;

	            while((num==0) ){
	                System.out.println("Vuelva a introducir el n�mero por favor, recuerde que no est� permitidos el 0.");
	                num=lector.nextInt();
	            }
	            if(num>=1) {
	                positivo=num;
	                SumaPositivos=SumaPositivos+positivo;
	                a++;
	                if(num%2==0) {
	                    par=num;
	                    SumaParP= SumaParP + par;
	                    c++;
	                }else {
	                    impar=num;
	                    SumaImparP= SumaImparP+impar;
	                    d++;
	                }
	            }else if(num<=-1) {
	                negativo=num;
	                SumaNegativos=SumaNegativos+negativo;
	                b++;
	                if(num%2==0) {
	                    par=num;
	                    SumaParN= SumaParN + par;
	                    c++;
	                }else {
	                    impar=num;
	                    SumaImparN= SumaImparN+impar;
	                    d++;
	                }
	            }
	            String Palabra = "No";
	            System.out.println("�Le gustar�a introducir otro n�mero? \n"
	                    + "(Solo se aceptar� la palabra NO como negaci�n)");
	            String Confirmaci�n=lector.next();

	            if(Palabra.equalsIgnoreCase(Confirmaci�n)) {
	                System.out.println("De acuerdo, estos son los resultados:");
	                seguir = false;
	            }
	        }while(seguir == true);
	        System.out.println("La suma de los positivos es: " + SumaPositivos + " y de los negativos: " + SumaNegativos + ", por contra la suma de los pares es: (" + " positivos " + SumaParP+ " y negativos " + SumaParN + " ) y de los impares: ( " + "positivos: " + SumaImparP + " negativos :" + SumaImparN + " )\n"
	                + "La cantidad de positivos es: " + a + ", de negativos: " + b  + ", pares: " + c + " e impares: " + d + "\n"
	                + "Por �ltimo la media de todos los n�meros introducidos es: " + (mediana/i)  );


	        lector.close();

	}

}
