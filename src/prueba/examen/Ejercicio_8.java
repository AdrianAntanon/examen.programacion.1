package prueba.examen;

import java.util.Scanner;

public class Ejercicio_8 {

	public static void main(String[] args) {
		 Scanner lector=new Scanner(System.in);

	        int num;
	        int numSecreto=(int) (Math.random()*100);
	        int i=1;
	        System.out.println("Bienvenido al juego de adivinaci�n de n�mero, recuerda que es entre 1 y 100\n"
	                + "Introduzca el n�mero, por favor:");
	        num=lector.nextInt();
	        do {
	            i++;

	            while(num<1 || num>100 ){
	                System.out.println("N�mero no autorizado, introduzca entre 1 y 100, por favor:");
	                num=lector.nextInt();
	            }
	            if(numSecreto > num) {
	                System.out.println("El n�mero a adivinar es m�s grande");
	            }else if(numSecreto<num){
	                System.out.println("El n�mero a adivinar es m�s peque�o");
	            }
	            System.out.println("Introduzca el n�mero, por favor: ");
	            num=lector.nextInt();

	        }while(numSecreto!=num);
	        lector.close();
	        System.out.println("Felicidades!\n"
	                + "Lo has conseguido en " + i + " intento/s");
    }

}


