package prueba.examen;

import java.util.Scanner;

public class Strings_1 {

	public static void main(String[] args) {
		Scanner lector = new Scanner (System.in);
		
		String frase = "";
		
		System.out.println("Introduce una frase, por favor");
		frase=lector.nextLine();
		
		System.out.println("La frase tiene un total de " + frase.length() + " posiciones");
		
		lector.close();

	}

}
