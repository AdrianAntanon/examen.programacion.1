package prueba.examen;

import java.util.Scanner;

public class Strings_6 {

	public static void main(String[] args) {
		Scanner lector=new Scanner (System.in);
		
		int i=0, posicion;
		
		System.out.println("Buenaos d�as \n"
				+ "El programa ordenar� alfab�ticamente las palabras que introduzcas a continuaci�n");
		
		String[] arrayEjemplo = new String [2];
		
		for(i=0;i<2;i++) {			
			System.out.println("Introduce la frase " + (i+1) +", por favor: ");
			arrayEjemplo[i]=lector.nextLine();
		}
		
		lector.close();
		
		posicion=arrayEjemplo[0].compareToIgnoreCase(arrayEjemplo[1]);
		
		System.out.println("La lista quedar�a ordenada de la siguiente forma:");
		
		if(posicion < 0) {
			System.out.println("1.- "+ arrayEjemplo[0] +" \n"
					+ "2.- " + arrayEjemplo[1]);
		}else if(posicion > 0) {
			System.out.println("1.- "+ arrayEjemplo[1] +" \n"
					+ "2.- " + arrayEjemplo[0]);
		}else {
			System.out.println("Las palabras o frases son iguales");
			System.out.println("1.- "+ arrayEjemplo[1] +" \n"
					+ "2.- " + arrayEjemplo[0]);
		}
		
		
	
	

	}

}
