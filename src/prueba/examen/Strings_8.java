package prueba.examen;

import java.util.Scanner;

public class Strings_8 {

	public static void main(String[] args) {
		
		Scanner lector=new Scanner (System.in);
		
		System.out.println("Introduce una frase, por favor: ");
		String frase=lector.nextLine();
		
		System.out.println("Y ahora vamos a buscar la palabra HOLA dentro de esa frase ");
		String palabra="hola";
		
		String [] array = frase.split(" ");

		
		int i=0, contador=0;
		int posicion=frase.toLowerCase().indexOf(palabra);
		
		for(i=0;i<array.length;i++) {
			
			if(array[i].equalsIgnoreCase(palabra)) {
				contador++;
			}
			
		}
		
		if(posicion != -1) {
			System.out.println("La palabra hola s� que figura dentro de la frase y comienza en la posici�n " + posicion + " hasta acabar en la " + (posicion+3) + "\n"
					+ "Adem�s dentro de la frase //" + frase + "\\ se ha encontrado la palabra //" + palabra + "\\ repetida una/s " + contador + " veces en distintas posiciones");
		}else {
			System.out.println("No se encontr� la palabra introducida, lo siento mucho.");
		}
		
		
		lector.close();

	}

}
